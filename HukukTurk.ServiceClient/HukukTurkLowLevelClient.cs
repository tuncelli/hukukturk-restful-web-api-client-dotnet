﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace HukukTurk.ServiceClient
{
    /// <summary>
    /// Provides HukukTurk API access for Low-Level operation
    /// </summary>
    public class HukukTurkLowLevelClient : IDisposable
    {
        public static Uri BaseUri { get; set; } = new Uri("https://www.hukukturk.com/api/v1/");
        private readonly HttpClient client;

        #region Constructors

        private HukukTurkLowLevelClient(HttpMessageHandler handler, MediaTypeWithQualityHeaderValue defaultAccept = null)
        {
            client = new HttpClient(handler, true);
            client.DefaultRequestHeaders.Accept.Add(defaultAccept ?? new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public HukukTurkLowLevelClient(string appId, string userId, string userSecret, MediaTypeWithQualityHeaderValue defaultAccept = null)
            : this(new AuthDelegatingHandler(appId, userId, userSecret), defaultAccept)
        {
        }

        public HukukTurkLowLevelClient(string appId, string secret, MediaTypeWithQualityHeaderValue defaultAccept = null)
            : this(new AuthDelegatingHandler(appId, secret), defaultAccept)
        {
        }

        #endregion

        #region Main call

        /// <summary>
        /// Main call
        /// </summary>
        /// <param name="method"></param>
        /// <param name="uri"></param>
        /// <param name="ct"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        /// <exception cref="OperationCanceledException"></exception>
        public Task<HttpResponseMessage> CallAsync(HttpMethod method, Uri uri, HttpContent content = null,
            CancellationToken ct = default)
        {
            if (method == null)
            {
                throw new ArgumentNullException(nameof(method));
            }

            if (uri == null)
            {
                throw new ArgumentNullException(nameof(uri));
            }

            switch (method.Method)
            {
                case "GET":
                    return client.GetAsync(uri, ct);
                case "DELETE":
                    return client.DeleteAsync(uri, ct);
                case "POST":
                    return client.PostAsync(uri, content, ct);
                default:
                    throw new ArgumentOutOfRangeException(nameof(method.Method),
                        $"HttpMethod Not Supported: {method.Method}");
            }
        }

        /// <summary>
        /// Main call
        /// </summary>
        /// <param name="method"></param>
        /// <param name="uri"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public HttpResponseMessage Call(HttpMethod method, Uri uri, HttpContent content = null)
        {
            return CallAsync(method, uri, content).GetAwaiter().GetResult();
        }

        #endregion

        #region IDisposable

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                client?.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}