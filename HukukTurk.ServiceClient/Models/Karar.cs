﻿using System;
using System.Runtime.Serialization;

namespace HukukTurk.ServiceClient.Models
{
    [DataContract(Name = "Karar", Namespace = "")]
    public class Karar : IContentKarar
    {
        private Karar()
        {
        }

        /// <summary>
        /// References on Option/KararTuru
        /// </summary>
        [DataMember]
        public int KararTuru { get; private set; }

        /// <summary>
        /// References on Option/Merci
        /// </summary>
        [DataMember]
        public int Merci { get; private set; }

        /// <summary>
        /// References on
        /// Option/UyusmazlikTuru
        /// or
        /// Option/AmkDavaTuru
        /// </summary>
        [DataMember]
        public int? DavaTuru { get; private set; }

        [DataMember]
        public string EsasNo1 { get; private set; }

        [DataMember]
        public string EsasNo2 { get; private set; }

        [DataMember]
        public string KararNo1 { get; private set; }

        [DataMember]
        public string KararNo2 { get; private set; }

        [DataMember]
        public DateTime KararTarihi { get; private set; }

        [DataMember]
        public string Ozu { get; private set; }
    }
}