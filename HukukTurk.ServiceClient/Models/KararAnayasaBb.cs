﻿using System;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace HukukTurk.ServiceClient.Models
{
    [DataContract(Name = "KararAnayasaBb", Namespace = "")]
    public class KararAnayasaBb : IContent
    {
        private KararAnayasaBb()
        {
        }

        /// <summary>
        /// References on Option/KararTuru
        /// </summary>
        [DataMember]
        public int KararTuru { get; private set; }

        /// <summary>
        /// References on Option/Merci
        /// </summary>
        [DataMember]
        public int Merci { get; private set; }

        [DataMember]
        public string Baslik { get; private set; }

        /// <summary>
        /// References on Option/AmkBbDavaTuru
        /// </summary>
        [DataMember]
        public ReadOnlyCollection<int> DavaTuru { get; private set; }

        [DataMember]
        public string BasvuruNo1 { get; private set; }

        [DataMember]
        public string BasvuruNo2 { get; private set; }

        [DataMember]
        public DateTime KararTarihi { get; private set; }

        [DataMember]
        public string Ozu { get; private set; }
    }
}