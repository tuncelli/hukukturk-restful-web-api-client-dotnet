﻿namespace HukukTurk.ServiceClient.Models
{
    public enum OptionName
    {
        Katalog,
        KararTuru,
        MevzuatTuru,
        UyusmazlikTuru,
        AmkDavaTuru,
        AmkBbDavaTuru,
        MaddeTuru,
        SayfaBoyu
    }
}