﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace HukukTurk.ServiceClient.Models
{
    [CollectionDataContract(Name = "Options", ItemName = "Opt",
        KeyName = "Key", ValueName = "Value", Namespace = "")]
    public sealed class OptionsResponse : Dictionary<int, string>, IResponse
    {
        private OptionsResponse()
        {
        }

        #region IResponse

        public bool IsSuccess { get; set; }
        public int StatusCode { get; set; }
        public string Reason { get; set; }
        public string ErrorMessage { get; set; }

        #endregion
    }
}