﻿using System.Collections.Generic;

namespace HukukTurk.ServiceClient.Models
{
    public class GetUsersResponse : List<string>, IResponse
    {
        private GetUsersResponse()
        {
        }

        #region IResponse

        public bool IsSuccess { get; set; }
        public int StatusCode { get; set; }
        public string Reason { get; set; }
        public string ErrorMessage { get; set; }

        #endregion
    }
}