﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace HukukTurk.ServiceClient.Models
{
    [CollectionDataContract(Name = "ResultSet", ItemName = "Madde", Namespace = "")]
    public sealed class MaddeListesiResponse : List<Madde>, IResponse
    {
        private MaddeListesiResponse()
        {
        }

        #region IResponse

        public bool IsSuccess { get; set; }
        public int StatusCode { get; set; }
        public string Reason { get; set; }
        public string ErrorMessage { get; set; }

        #endregion
    }
}