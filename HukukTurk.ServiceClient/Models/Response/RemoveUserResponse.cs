﻿using System.Net.Http;

namespace HukukTurk.ServiceClient.Models
{
    public class RemoveUserResponse : IResponse
    {
        internal RemoveUserResponse(HttpResponseMessage response)
        {
            IsSuccess = response.IsSuccessStatusCode;
            StatusCode = (int) response.StatusCode;
            Reason = response.ReasonPhrase;
        }

        #region IResponse

        public bool IsSuccess { get; set; }
        public int StatusCode { get; set; }
        public string Reason { get; set; }
        public string ErrorMessage { get; set; }

        #endregion
    }
}