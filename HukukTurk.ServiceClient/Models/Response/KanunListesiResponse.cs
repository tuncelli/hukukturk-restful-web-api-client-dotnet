﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace HukukTurk.ServiceClient.Models
{
    [CollectionDataContract(Name = "ResultSet", Namespace = "",
        ItemName = "Mevzuat", KeyName = "MevzuatId", ValueName = "Description")]
    public sealed class KanunListesiResponse : Dictionary<int, string>, IResponse
    {
        private KanunListesiResponse()
        {
        }

        #region IResponse

        public bool IsSuccess { get; set; }
        public int StatusCode { get; set; }
        public string Reason { get; set; }
        public string ErrorMessage { get; set; }

        #endregion
    }
}