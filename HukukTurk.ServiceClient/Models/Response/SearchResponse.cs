﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace HukukTurk.ServiceClient.Models
{
    [DataContract(Name = "SearchResult", Namespace = ""), JsonObject]
    public class SearchResponse<T> : IEnumerable<SearchResponse<T>.Item>, IResponse where T : IContent
    {
        private SearchResponse()
        {
        }

        #region IResponse

        public bool IsSuccess { get; set; }
        public int StatusCode { get; set; }
        public string Reason { get; set; }
        public string ErrorMessage { get; set; }

        #endregion

        #region DataMembers

        [DataMember]
        public int Count { get; private set; }

        [DataMember]
        public int PageSize { get; private set; }

        [DataMember]
        public IEnumerable<Item> ResultSet { get; private set; }

        #endregion

        #region IEnumerable

        public IEnumerator<Item> GetEnumerator()
        {
            return ResultSet.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region Item

        [DataContract(Name = "Item", Namespace = "")]
        public class Item
        {
            private Item()
            {
            }

            #region DataMembers

            [DataMember]
            public T Content { get; private set; }

            [DataMember]
            public string Snippet { get; private set; }

            [DataMember]
            public string Url { get; private set; }

            #endregion
        }

        #endregion
    }
}