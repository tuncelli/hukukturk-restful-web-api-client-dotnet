﻿using System.Runtime.Serialization;

namespace HukukTurk.ServiceClient.Models
{
    public class AddUserResponse : IResponse
    {
        private AddUserResponse()
        {
        }

        public string UserId { get; set; }
        public string UserSecret { get; set; }

        #region IResponse

        public bool IsSuccess { get; set; }
        public int StatusCode { get; set; }
        public string Reason { get; set; }
        public string ErrorMessage { get; set; }

        #endregion

        public static implicit operator AddUserResponse(string responseContent)
        {
            string[] temp = responseContent.Split(':');
            return new AddUserResponse {UserId = temp[0], UserSecret = temp[1]};
        }
    }
}