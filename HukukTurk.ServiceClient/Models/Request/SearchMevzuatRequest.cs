﻿using System;
using System.Collections.Generic;

namespace HukukTurk.ServiceClient.Models
{
    public class SearchMevzuatRequest : IRequest
    {
        public int[] MevzuatTuru { get; set; }

        public string Sorgu { get; set; }

        public string Numarasi { get; set; }

        public int? RgSayisi { get; set; }

        public DateTime? KabulTarihiBaslangic { get; set; }

        public DateTime? KabulTarihiBitis { get; set; }

        public int? Siralama { get; set; }

        public bool? DogalDil { get; set; }

        public bool? OnIzleme { get; set; }

        public int? Sayfa { get; set; }

        public int? SayfaBoyu { get; set; }

        public override string ToString()
        {
            var dic = new Dictionary<string, object>
            {
                {"mevzuatturucommaseparated", MevzuatTuru},
                {"q", Sorgu},
                {"numarasi", Numarasi},
                {"rgsayisi", RgSayisi},
                {"kabultarihibaslangic", KabulTarihiBaslangic},
                {"kabultarihibitis", KabulTarihiBitis},
                {"siralama", Siralama},
                {"dogaldil", DogalDil},
                {"onizleme", OnIzleme},
                {"sayfa", Sayfa},
                {"sayfaboyu", SayfaBoyu}
            };
            return UriHelper.BuildQuery(dic);
        }
    }
}