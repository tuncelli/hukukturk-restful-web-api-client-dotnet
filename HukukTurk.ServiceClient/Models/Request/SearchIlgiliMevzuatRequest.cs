﻿using System;
using System.Collections.Generic;

namespace HukukTurk.ServiceClient.Models
{
    public class SearchIlgiliMevzuatRequest : IRequest
    {
        public SearchIlgiliMevzuatRequest(string metin)
        {
            if (metin == null) throw new ArgumentNullException(nameof(metin));
            if (metin.Trim().Length == 0) throw new ArgumentException("Karakter dizisi boş olamaz.", nameof(metin));
            Metin = metin;
        }

        public string Metin { get; }

        public int[] MevzuatTuru { get; set; }

        public bool? Mulga { get; set; }

        public bool? DegisiklikYapan { get; set; }

        public int? Sayfa { get; set; }

        public int? SayfaBoyu { get; set; }

        public override string ToString()
        {
            var dic = new Dictionary<string, object>
            {
                {"mevzuatturucommaseparated", MevzuatTuru},
                {"mulga", Mulga},
                {"degisiklikyapan", DegisiklikYapan},
                {"sayfa", Sayfa},
                {"sayfaboyu", SayfaBoyu}
            };
            return UriHelper.BuildQuery(dic);
        }
    }
}