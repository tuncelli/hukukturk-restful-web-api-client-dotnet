﻿using System;
using System.Collections.Generic;

namespace HukukTurk.ServiceClient.Models
{
    public class SearchKararAnayasaBbRequest : IRequest
    {
        public string Sorgu { get; set; }

        public string BasvuruNo1 { get; set; }

        public string BasvuruNo2 { get; set; }

        public DateTime? KararTarihiBaslangic { get; set; }

        public DateTime? KararTarihiBitis { get; set; }

        public int[] DavaTuru { get; set; }

        public int? Siralama { get; set; }

        public bool? DogalDil { get; set; }

        public bool? OnIzleme { get; set; }

        public int[] Merci { get; set; }

        public int? Sayfa { get; set; }

        public int? SayfaBoyu { get; set; }

        public override string ToString()
        {
            var dic = new Dictionary<string, object>
            {
                {"q", Sorgu},
                {"basvuruno1", BasvuruNo1},
                {"basvuruno2", BasvuruNo2},
                {"karartarihibaslangic", KararTarihiBaslangic},
                {"karartarihibitis", KararTarihiBitis},
                {"davaturucommaseparated", DavaTuru},
                {"siralama", Siralama},
                {"dogaldil", DogalDil},
                {"onizleme", OnIzleme},
                {"mercicommaseparated", Merci},
                {"sayfa", Sayfa},
                {"sayfaboyu", SayfaBoyu}
            };
            return UriHelper.BuildQuery(dic);
        }
    }
}