﻿using System;
using System.Collections.Generic;

namespace HukukTurk.ServiceClient.Models
{
    public class SearchKararRequest : IRequest
    {
        public int? KararTuru { get; set; }

        public string Sorgu { get; set; }

        public string EsasNo1 { get; set; }

        public string EsasNo2 { get; set; }

        public string KararNo1 { get; set; }

        public string KararNo2 { get; set; }

        public DateTime? KararTarihiBaslangic { get; set; }

        public DateTime? KararTarihiBitis { get; set; }

        public int[] Merciler { get; set; }

        public int? DavaTuru { get; set; }

        public int? UyusmazlikTuru { get; set; }

        public int? Siralama { get; set; }

        public bool? DogalDil { get; set; }

        public bool? OnIzleme { get; set; }

        public int? Sayfa { get; set; }

        public int? SayfaBoyu { get; set; }

        public override string ToString()
        {
            var dic = new Dictionary<string, object>
            {
                {"kararturu", KararTuru},
                {"q", Sorgu},
                {"esasno1", EsasNo1},
                {"esasno2", EsasNo2},
                {"kararno1", KararNo1},
                {"kararno2", KararNo2},
                {"karartarihibaslangic", KararTarihiBaslangic},
                {"karartarihibitis", KararTarihiBitis},
                {"siralama", Siralama},
                {"uyusmazlikturu", UyusmazlikTuru},
                {"davaturu", DavaTuru},
                {"dogaldil", DogalDil},
                {"onizleme", OnIzleme},
                {"mercicommaseparated", Merciler},
                {"sayfa", Sayfa},
                {"sayfaboyu", SayfaBoyu}
            };
            return UriHelper.BuildQuery(dic);
        }
    }
}