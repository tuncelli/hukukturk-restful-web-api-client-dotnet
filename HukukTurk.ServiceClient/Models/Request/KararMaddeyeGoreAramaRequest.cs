﻿using System.Collections.Generic;

namespace HukukTurk.ServiceClient.Models
{
    public class KararMaddeyeGoreAramaRequest : IRequest
    {
        public int KararTuru { get; set; }

        public int MevzuatId { get; set; }

        public short? MaddeNo { get; set; }

        public int? MaddeTuru { get; set; }

        public int? Merci { get; set; }

        public int? Sayfa { get; set; }

        public int? SayfaBoyu { get; set; }

        public override string ToString()
        {
            var dic = new Dictionary<string, object>
            {
                {"kararturu", KararTuru},
                {"mevzuatid", MevzuatId},
                {"maddeno", MaddeNo},
                {"maddeturu", MaddeTuru},
                {"merci", Merci},
                {"sayfa", Sayfa},
                {"sayfaboyu", SayfaBoyu},
            };
            return UriHelper.BuildQuery(dic);
        }
    }
}