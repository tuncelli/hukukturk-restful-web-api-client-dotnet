﻿using System;
using System.Collections.Generic;

namespace HukukTurk.ServiceClient.Models
{
    public class SearchKararAihmRequest :  IRequest
    {
        public string Sorgu { get; set; }

        public string BasvuruNo { get; set; }

        public DateTime? KararTarihiBaslangic { get; set; }

        public DateTime? KararTarihiBitis { get; set; }

        public int? Siralama { get; set; }

        public bool? DogalDil { get; set; }

        public bool? OnIzleme { get; set; }

        public int? Sayfa { get; set; }

        public int? SayfaBoyu { get; set; }

        public override string ToString()
        {
            var dic = new Dictionary<string, object>
            {
                {"q", Sorgu},
                {"basvuruno", BasvuruNo},
                {"karartarihibaslangic", KararTarihiBaslangic},
                {"karartarihibitis", KararTarihiBitis},
                {"siralama", Siralama},
                {"dogaldil", DogalDil},
                {"onizleme", OnIzleme},
                {"sayfa", Sayfa},
                {"sayfaboyu", SayfaBoyu}
            };
            return UriHelper.BuildQuery(dic);
        }
    }
}