﻿using System;
using System.Collections.Generic;

namespace HukukTurk.ServiceClient.Models
{
    public class SearchIlgiliKararlarRequest : IRequest
    {
        public SearchIlgiliKararlarRequest(string metin)
        {
            if (metin == null) throw new ArgumentNullException(nameof(metin));
            if (metin.Trim().Length == 0) throw new ArgumentException("Karakter dizisi boş olamaz.", nameof(metin));
            Metin = metin;
        }

        public string Metin { get; }

        public int[] Merciler { get; set; }

        public int? Sayfa { get; set; }

        public int? SayfaBoyu { get; set; }

        public override string ToString()
        {
            var dic = new Dictionary<string, object>
            {
                {"mercicommaseparated", Merciler},
                {"sayfa", Sayfa},
                {"sayfaboyu", SayfaBoyu}
            };
            return UriHelper.BuildQuery(dic);
        }
    }
}
