﻿using System;
using System.Runtime.Serialization;

namespace HukukTurk.ServiceClient.Models
{
    [DataContract(Name = "Mevzuat", Namespace = "")]
    public class Mevzuat : IContentMevzuat
    {
        private Mevzuat()
        {
        }

        /// <summary>
        /// References on Option/MevzuatTuru
        /// </summary>
        [DataMember]
        public int MevzuatTuru { get; private set; }

        /// <summary>
        /// Başlığı
        /// </summary>
        [DataMember]
        public string Baslik { get; private set; }

        /// <summary>
        /// Numarası
        /// </summary>
        [DataMember]
        public string Numarasi { get; private set; }

        /// <summary>
        /// Kabul tarihi
        /// </summary>
        [DataMember]
        public DateTime? KabulTarihi { get; private set; }

        // TODO: Referans metodu eklenecek.
        [DataMember]
        public int? Merci { get; private set; }

        /// <summary>
        /// Resmi Gazete tarihi
        /// </summary>
        [DataMember]
        public DateTime? RgTarihi { get; private set; }

        /// <summary>
        /// Resmi Gazete sayısı
        /// </summary>
        [DataMember]
        public int? RgSayisi { get; private set; }

        /// <summary>
        /// Resmi Gazete mükerrer sayısı
        /// </summary>
        [DataMember]
        public byte? RgMukerrer { get; private set; }

        /// <summary>
        /// Başka bir mevzuatı değiştiren bir mevzuat mı? Değer null ise bilgi yok veya değişiklik yapmayan mevzuat.
        /// </summary>
        [DataMember]
        public bool? DegisiklikYapan { get; private set; }

        /// <summary>
        /// Yürürlükten kalkmış bir mevzuat mı? Değer null ise bilgi yok veya halen yürürlükte.
        /// </summary>
        [DataMember]
        public bool? Mulga { get; private set; }
    }
}