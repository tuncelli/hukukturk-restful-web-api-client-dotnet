﻿using System;

namespace HukukTurk.ServiceClient.Models
{
    public interface IContentMevzuat : IContent
    {
        int MevzuatTuru { get; }

        string Baslik { get; }

        string Numarasi { get; }

        DateTime? KabulTarihi { get; }

        int? Merci { get; }

        DateTime? RgTarihi { get; }

        int? RgSayisi { get; }

        byte? RgMukerrer { get; }

        bool? DegisiklikYapan { get; }

        bool? Mulga { get; }
    }
}