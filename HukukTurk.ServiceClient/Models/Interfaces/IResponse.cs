﻿namespace HukukTurk.ServiceClient.Models
{
    public interface IResponse
    {
        /// <summary>Gets or sets a value that indicates if the HTTP response was successful.</summary>
        bool IsSuccess { get; set; }

        /// <summary>Gets or sets the status code of the HTTP response.</summary>
        int StatusCode { get; set; }

        /// <summary>Gets or sets the reason phrase which typically is sent by servers together with the status code.</summary>
        string Reason { get; set; }

        /// <summary>Gets or sets the message returned from server if the HTTP response was unsuccessful.</summary>
        string ErrorMessage { get; set; }
    }
}