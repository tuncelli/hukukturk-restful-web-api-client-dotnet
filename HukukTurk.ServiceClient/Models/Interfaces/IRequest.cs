﻿namespace HukukTurk.ServiceClient.Models
{
    public interface IRequest
    {
        int? Sayfa { get; set; }

        int? SayfaBoyu { get; set; }
    }
}
