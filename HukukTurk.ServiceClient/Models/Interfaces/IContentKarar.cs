﻿using System;

namespace HukukTurk.ServiceClient.Models
{
    public interface IContentKarar : IContent
    {
        int KararTuru { get; }

        int Merci { get; }

        int? DavaTuru { get; }

        string EsasNo1 { get; }

        string EsasNo2 { get; }

        string KararNo1 { get; }

        string KararNo2 { get; }

        DateTime KararTarihi { get; }

        string Ozu { get; }
    }
}