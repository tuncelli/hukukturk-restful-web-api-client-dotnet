﻿using System;
using System.Runtime.Serialization;

namespace HukukTurk.ServiceClient.Models
{
    [DataContract(Name = "KararAihm", Namespace = "")]
    public class KararAihm : IContent
    {
        private KararAihm()
        {
        }

        /// <summary>
        /// References on Option/KararTuru
        /// </summary>
        [DataMember]
        public int KararTuru { get; private set; }

        [DataMember]
        public string Baslik { get; private set; }

        [DataMember]
        public string BasvuruNo { get; private set; }

        [DataMember]
        public DateTime KararTarihi { get; private set; }

        [DataMember]
        public string Ozu { get; private set; }
    }
}