﻿using System.Runtime.Serialization;

namespace HukukTurk.ServiceClient.Models
{
    // ReSharper disable once ClassCannotBeInstantiated
    [DataContract(Namespace = "")]
    public class Madde
    {
        private Madde()
        {
        }

        [DataMember]
        public short MaddeNo { get; private set; }

        [DataMember]
        public int MaddeTuru { get; private set; }

        [DataMember]
        public string Baslik { get; private set; }
    }
}