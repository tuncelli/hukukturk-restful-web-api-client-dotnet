﻿using System.Diagnostics.CodeAnalysis;
#if NET40
using System.IO;
#endif
using System.Net.Http;
using System.Threading.Tasks;

namespace HukukTurk.ServiceClient
{
    [SuppressMessage("AsyncFixer01", "AsyncFixer01")]
    internal static class HttpContentExtensions
    {
        /// <summary>
        /// Workaround for ReadAsStringAsync() hangs on IIS in .NET Framework 4.0
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        public static async Task<string> ReadAsStringAsyncFixedAsync(this HttpContent content)
        {
#if NET40
            using (Stream stream = await content.ReadAsStreamAsync().ConfigureAwait(false))
            using (var reader = new StreamReader(stream))
            {
                return await reader.ReadToEndAsync().ConfigureAwait(false);
            }
#else
			return await content.ReadAsStringAsync().ConfigureAwait(false);
#endif
        }
    }
}