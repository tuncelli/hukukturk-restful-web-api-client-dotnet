﻿using HukukTurk.ServiceClient.Models;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace HukukTurk.ServiceClient
{
    /// <summary>
    /// Provides HukukTurk API access for End-User Management (Administration)
    /// </summary>
    public class HukukTurkHighLevelAdminClient : IDisposable
    {
        public HukukTurkLowLevelClient LowLevelClient { get; }

        public HukukTurkHighLevelAdminClient(string appId, string secret)
        {
            LowLevelClient = new HukukTurkLowLevelClient(appId, secret,
                new MediaTypeWithQualityHeaderValue("application/json"));
        }

        #region Async

        /// <summary>
        /// Retrieves all user id and user secrets
        /// </summary>
        /// <param name="ct"></param>
        /// <returns>Array of user id and user secrets separated by a colon character</returns>
        /// <exception cref="OperationCanceledException"></exception>
        public async Task<GetUsersResponse> GetUsersAsync(CancellationToken ct = default)
        {
            Uri uri = UriHelper.BuildUri(HukukTurkLowLevelClient.BaseUri, "user", "getallusers");
            HttpResponseMessage response =
                await LowLevelClient.CallAsync(HttpMethod.Get, uri, ct: ct).ConfigureAwait(false);
            return await response.DeserializeAsync<GetUsersResponse>().ConfigureAwait(false);
        }

        /// <summary>
        /// Adds a new user and returns its user id
        /// </summary>
        /// <param name="ct"></param>
        /// <returns>user id for the new user</returns>
        /// <exception cref="OperationCanceledException"></exception>
        public async Task<AddUserResponse> AddUserAsync(CancellationToken ct = default)
        {
            Uri uri = UriHelper.BuildUri(HukukTurkLowLevelClient.BaseUri, "user", "add");
            HttpResponseMessage response =
                await LowLevelClient.CallAsync(HttpMethod.Get, uri, ct: ct).ConfigureAwait(false);
            return await response.DeserializeAsync<AddUserResponse>().ConfigureAwait(false);
        }

        /// <summary>
        /// Removes specified user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="ct"></param>
        /// <returns>Returns true if the user successfully removed</returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="OperationCanceledException"></exception>
        public async Task<RemoveUserResponse> RemoveUserAsync(string userId, CancellationToken ct = default)
        {
            string query = $"userId={UriHelper.EscapeDataString(userId)}";
            Uri uri = UriHelper.BuildUri(HukukTurkLowLevelClient.BaseUri, "user", "remove", query);
            HttpResponseMessage response =
                await LowLevelClient.CallAsync(HttpMethod.Delete, uri, ct: ct).ConfigureAwait(false);
            return new RemoveUserResponse(response);
        }

        #endregion

        #region Sync

        /// <summary>
        /// Retrieves all user id and user secrets
        /// </summary>
        /// <returns>Array of user id and user secrets separated by a colon character</returns>
        public GetUsersResponse GetUsers()
        {
            return GetUsersAsync().GetAwaiter().GetResult();
        }

        /// <summary>
        /// Adds a new user and returns its user id
        /// </summary>
        /// <returns>user id for the new user</returns>
        public AddUserResponse AddUser()
        {
            return AddUserAsync().GetAwaiter().GetResult();
        }

        /// <summary>
        /// Removes specified user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>Returns true if the user successfully removed</returns>
        /// <exception cref="ArgumentNullException"></exception>
        public RemoveUserResponse RemoveUser(string userId)
        {
            return RemoveUserAsync(userId).GetAwaiter().GetResult();
        }

        #endregion

        #region IDisposable

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                LowLevelClient?.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}