﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
#if NET40
using System.Text.RegularExpressions;
#endif

namespace HukukTurk.ServiceClient
{
    public static class UriHelper
    {
        public static string EscapeDataString(string stringToEscape)
        {
#if NET40
            return Regex.Replace(Uri.EscapeDataString(stringToEscape.ToLowerInvariant()), @"[\!*\'\(\)]",
                m => Uri.HexEscape(Convert.ToChar(m.Value[0].ToString()))).ToLowerInvariant();
#else
			// NET 4.5+
			return Uri.EscapeDataString(stringToEscape.ToLowerInvariant()).ToLowerInvariant();
#endif
        }

        public static string BuildQuery(IDictionary<string, object> pparams)
        {
            Dictionary<string, string> d = pparams.ToDictionary(n => n.Key,
                n =>
                {
                    if (n.Value == null)
                    {
                        return null;
                    }

                    object ret = n.Value;
                    if (ret is DateTime dt)
                    {
                        ret = dt.ToString("yyyy-MM-dd");
                    }
                    else if (!(ret is string) && ret is IEnumerable iterable)
                    {
                        ret = String.Join(",", iterable.Cast<object>().Where(a => a != null));
                    }

                    return EscapeDataString(ret.ToString());
                });
            if (d.Count == 0)
            {
                return string.Empty;
            }

            return String.Join("&",
                from kvp in d
                where kvp.Value != null
                select String.Join("=", kvp.Key, kvp.Value));
        }

        public static Uri BuildUri(Uri baseUri, string methodPath, string methodName, string query = null,
            bool pretty = false)
        {
            StringBuilder s = new StringBuilder();
            if (!String.IsNullOrWhiteSpace(methodPath))
            {
                methodPath = methodPath.Trim('/');
                s.Append(EscapeDataString(methodPath));
                s.Append('/');
            }

            if (!String.IsNullOrWhiteSpace(methodName))
            {
                s.Append(EscapeDataString(methodName));
            }

            if (!String.IsNullOrWhiteSpace(query))
            {
                s.Append('?');
                s.Append(query);
                if (pretty)
                {
                    s.Append("&pretty=true");
                }
            }
            else if (pretty)
            {
                s.Append("?pretty=true");
            }

            return new Uri(baseUri, s.ToString());
        }
    }
}