﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HukukTurk.ServiceClient
{
    internal class AuthDelegatingHandler : DelegatingHandler
    {
        public const string AuthorizationHeaderScheme = "amx";

        private readonly string appId;
        private readonly string userId;
        private readonly byte[] secretBytes;

        public AuthDelegatingHandler(string appId, string secret)
            : base(new HttpClientHandler())
        {
            if (appId == null)
            {
                throw new ArgumentNullException(nameof(appId));
            }

            if (appId.Length != 32)
            {
                throw new ArgumentException("app id length must be 32", nameof(appId));
            }

            this.appId = appId;

            if (secret == null)
            {
                throw new ArgumentNullException(nameof(secret));
            }

            secretBytes = Convert.FromBase64String(secret);
            if (secretBytes.Length != 32)
            {
                throw new ArgumentException("byte array length must be 32.", nameof(secretBytes));
            }
        }

        public AuthDelegatingHandler(string appId, string userId, string secret) : this(appId, secret)
        {
            if (userId == null)
            {
                throw new ArgumentNullException(nameof(userId));
            }

            if (userId.Length != 32)
            {
                throw new ArgumentException("user id length must be 32", nameof(appId));
            }

            this.userId = userId;
        }

        public void IgnoreCertificateValidationErrors(bool ignore = true)
        {
#if NETCOREAPP
			bool ServerCertificateCustomValidationCallback(HttpRequestMessage message, X509Certificate2 certificate2, X509Chain chain, SslPolicyErrors errors)
			{
				return true;
			}

			((HttpClientHandler) InnerHandler).ServerCertificateCustomValidationCallback = ignore
				? ServerCertificateCustomValidationCallback
				: (Func<HttpRequestMessage, X509Certificate2, X509Chain, SslPolicyErrors, bool>) null;
#else
            bool RemoteCertificateValidationCallback(object sender, X509Certificate certificate, X509Chain chain,
                SslPolicyErrors sslpolicyerrors)
            {
                return true;
            }

            ((WebRequestHandler) InnerHandler).ServerCertificateValidationCallback = ignore
                ? (RemoteCertificateValidationCallback) RemoteCertificateValidationCallback
                : null;
#endif
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            string requestUri = UriHelper.EscapeDataString(request.RequestUri.PathAndQuery);
            string requestHttpMethod = request.Method.Method;
            string requestContentBase64String = string.Empty;

            // Calculate UNIX time
            DateTime epochStart = new DateTime(1970, 01, 01, 0, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan timeSpan = DateTime.UtcNow - epochStart;
            string requestTimeStamp = Convert.ToUInt64(timeSpan.TotalSeconds).ToString();

            // Create random nonce for each request
            string nonce = Guid.NewGuid().ToString("N");

            // Checking if the request contains body, usually will be null wiht HTTP GET and DELETE
            if (request.Content != null)
            {
                byte[] content = await request.Content.ReadAsByteArrayAsync().ConfigureAwait(false);
                //Hashing the request body, any change in request body will result in different hash, we'll incure message integrity
                byte[] requestContentHash = CalculateMd5(content);
                requestContentBase64String = Convert.ToBase64String(requestContentHash);
            }

            // Creating the raw signature string
            string signatureRawData = Join(
                new[]
                {
                    appId,
                    userId,
                    requestHttpMethod,
                    requestUri,
                    requestTimeStamp,
                    nonce,
                    requestContentBase64String
                });

            Debug.WriteLine("Client Sig: " + signatureRawData);
            Debug.WriteLine("Client Key: " + Convert.ToBase64String(secretBytes));

            string requestSignatureBase64String = CalculateSignatureHash(signatureRawData);
            // Setting the values in the Authorization header using custom scheme (amx)
            string authzHeader = Join(new[]
            {
                appId,
                userId,
                requestSignatureBase64String,
                nonce,
                requestTimeStamp
            }, ":");

            request.Headers.Authorization = new AuthenticationHeaderValue(AuthorizationHeaderScheme, authzHeader);
            return await base.SendAsync(request, cancellationToken).ConfigureAwait(false);
        }

        private string CalculateSignatureHash(string plain)
        {
            byte[] plainBytes = Encoding.UTF8.GetBytes(plain);
            byte[] signatureBytes;
            using (HMACSHA256 hmac = new HMACSHA256(secretBytes))
            {
                signatureBytes = hmac.ComputeHash(plainBytes);
            }

            return Convert.ToBase64String(signatureBytes);
        }

        private static string Join(string[] args, string seperator = null)
        {
            if (args == null)
            {
                throw new ArgumentNullException(nameof(args));
            }

            return seperator == null
                ? String.Concat(args.Where(n => n != null))
                : String.Join(seperator, args.Where(n => n != null));
        }

        private static byte[] CalculateMd5(byte[] plainBytes)
        {
            using (MD5 md5 = MD5.Create())
            {
                return md5.ComputeHash(plainBytes);
            }
        }
    }
}