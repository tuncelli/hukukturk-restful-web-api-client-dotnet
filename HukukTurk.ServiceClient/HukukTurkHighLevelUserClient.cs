﻿using HukukTurk.ServiceClient.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace HukukTurk.ServiceClient
{
    /// <summary>
    /// Provides HukukTurk API methods for End-User
    /// </summary>
    public class HukukTurkHighLevelUserClient : IDisposable
    {
        public HukukTurkLowLevelClient LowLevelClient { get; }

        public HukukTurkHighLevelUserClient(string appId, string userId, string userSecret)
        {
            LowLevelClient = new HukukTurkLowLevelClient(appId, userId, userSecret,
                new MediaTypeWithQualityHeaderValue("application/json"));
        }

        #region Async Karar

        private async Task<SearchResponse<Karar>> SearchKararAsync(SearchKararRequest request, string methodName,
            CancellationToken ct = default)
        {
            Uri uri = UriHelper.BuildUri(HukukTurkLowLevelClient.BaseUri, "Search", methodName, request.ToString());
            HttpResponseMessage response =
                await LowLevelClient.CallAsync(HttpMethod.Get, uri, ct: ct).ConfigureAwait(false);
            return await response.DeserializeAsync<SearchResponse<Karar>>().ConfigureAwait(false);
        }

        /// <summary>
        /// Yargıtay Kararı Arama
        /// </summary>
        /// <param name="request"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="OperationCanceledException"></exception>
        public Task<SearchResponse<Karar>> SearchKararYargitayAsync(SearchKararRequest request,
            CancellationToken ct = default)
        {
            return SearchKararAsync(request, "Yargitay", ct);
        }

        /// <summary>
        ///     Danıştay Kararı Arama
        /// </summary>
        /// <param name="request"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="OperationCanceledException"></exception>
        public Task<SearchResponse<Karar>> SearchKararDanistayAsync(SearchKararRequest request,
            CancellationToken ct = default)
        {
            return SearchKararAsync(request, "Danistay", ct);
        }

        /// <summary>
        /// Anayasa Mahkemesi Kararı Arama
        /// </summary>
        /// <param name="request"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="OperationCanceledException"></exception>
        public Task<SearchResponse<Karar>> SearchKararAnayasaMahkemesiAsync(SearchKararRequest request,
            CancellationToken ct = default)
        {
            return SearchKararAsync(request, "AnayasaMahkemesi", ct);
        }

        /// <summary>
        ///     Uyuşmazlık Mahkemesi Kararı Arama
        /// </summary>
        /// <param name="request"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="OperationCanceledException"></exception>
        public Task<SearchResponse<Karar>> SearchKararUyusmazlikMahkemesiAsync(SearchKararRequest request,
            CancellationToken ct = default)
        {
            return SearchKararAsync(request, "UyusmazlikMahkemesi", ct);
        }

        /// <summary>
        /// Anayasa Mahkemesi Bireysel Başvuru Kararı Arama
        /// </summary>
        /// <param name="request"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="OperationCanceledException"></exception>
        public async Task<SearchResponse<KararAnayasaBb>> SearchKararAnayasaMahkemesiBbAsync(
            SearchKararAnayasaBbRequest request,
            CancellationToken ct = default)
        {
            Uri uri = UriHelper.BuildUri(HukukTurkLowLevelClient.BaseUri, "Search", "AnayasaMahkemesiBb",
                request.ToString());
            HttpResponseMessage response =
                await LowLevelClient.CallAsync(HttpMethod.Get, uri, ct: ct).ConfigureAwait(false);
            return await response.DeserializeAsync<SearchResponse<KararAnayasaBb>>().ConfigureAwait(false);
        }

        /// <summary>
        /// Avrupa İnsan Hakları Mahkemesi Kararı Arama
        /// </summary>
        /// <param name="request"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="OperationCanceledException"></exception>
        public async Task<SearchResponse<KararAihm>> SearchKararAihmAsync(
            SearchKararAihmRequest request,
            CancellationToken ct = default)
        {
            Uri uri = UriHelper.BuildUri(HukukTurkLowLevelClient.BaseUri, "Search", "Aihm", request.ToString());
            HttpResponseMessage response =
                await LowLevelClient.CallAsync(HttpMethod.Get, uri, ct: ct).ConfigureAwait(false);
            return await response.DeserializeAsync<SearchResponse<KararAihm>>().ConfigureAwait(false);
        }

        /// <summary>
        /// Bir metne göre Yargıtay, Danıştay, Anayasa Mahkemesi ve Uyuşmazlık Mahkemesi içerisinde ilgili karar araması yapar.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        public async Task<SearchResponse<Karar>> SearchIlgiliKararlarAsync(SearchIlgiliKararlarRequest request,
            CancellationToken ct = default)
        {
            Uri uri = UriHelper.BuildUri(HukukTurkLowLevelClient.BaseUri, "Search", "IlgiliKararlar",
                request.ToString());
            var pair = new KeyValuePair<string, string>(String.Empty, request.Metin);
            var content = new FormUrlEncodedContent(new[] { pair });
            HttpResponseMessage response = await LowLevelClient.CallAsync(HttpMethod.Post, uri, content, ct)
                .ConfigureAwait(false);
            return await response.DeserializeAsync<SearchResponse<Karar>>().ConfigureAwait(false);
        }

        #endregion

        #region Async Karar Maddeye Göre Arama

        /// <summary>
        /// Belirtilen karar türünde Kanun-Madde ilişkisine göre
        /// arama yapabilmek için Kanun listesini alır.
        /// </summary>
        /// <param name="kararTuru"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="OperationCanceledException"></exception>
        public async Task<KanunListesiResponse> GetKararMaddeyeGoreAramaKanunListesiAsync(int kararTuru,
            CancellationToken ct = default)
        {
            var query = $"kararturu={kararTuru}";
            Uri uri = UriHelper.BuildUri(HukukTurkLowLevelClient.BaseUri, "Search", "KararMevzuatListesi", query);
            HttpResponseMessage response =
                await LowLevelClient.CallAsync(HttpMethod.Get, uri, ct: ct).ConfigureAwait(false);
            return await response.DeserializeAsync<KanunListesiResponse>().ConfigureAwait(false);
        }

        /// <summary>
        /// Belirtilen karar türünde Kanun-Madde ilişkisine göre
        /// arama yapabilmek için belirtilen mevzuat için Madde listesini alır.
        /// </summary>
        /// <param name="kararTuru"></param>
        /// <param name="mevzuatId"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="OperationCanceledException"></exception>
        public async Task<MaddeListesiResponse> GetKararMaddeyeGoreAramaMaddeListesiAsync(int kararTuru,
            int mevzuatId, CancellationToken ct = default)
        {
            var query = $"kararturu={kararTuru}&mevzuatid={mevzuatId}";
            Uri uri = UriHelper.BuildUri(HukukTurkLowLevelClient.BaseUri, "Search", "KararMevzuatMaddeListesi", query);
            HttpResponseMessage response =
                await LowLevelClient.CallAsync(HttpMethod.Get, uri, ct: ct).ConfigureAwait(false);
            return await response.DeserializeAsync<MaddeListesiResponse>().ConfigureAwait(false);
        }

        /// <summary>
        /// Belirtilen karar türünde Kanun-Madde ilişkisine göre arama yapar.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="OperationCanceledException"></exception>
        public async Task<SearchResponse<Karar>> KararMaddeyeGoreAramaAsync(KararMaddeyeGoreAramaRequest request,
            CancellationToken ct = default)
        {
            Uri uri = UriHelper.BuildUri(HukukTurkLowLevelClient.BaseUri, "Search", "KararMadde", request.ToString());
            HttpResponseMessage response =
                await LowLevelClient.CallAsync(HttpMethod.Get, uri, ct: ct).ConfigureAwait(false);
            return await response.DeserializeAsync<SearchResponse<Karar>>().ConfigureAwait(false);
        }

        #endregion

        #region Async Mevzuat

        /// <summary>
        /// T.C. Mevzuatı Arama
        /// </summary>
        /// <param name="request"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="OperationCanceledException"></exception>
        public async Task<SearchResponse<Mevzuat>> SearchMevzuatAsync(SearchMevzuatRequest request,
            CancellationToken ct = default)
        {
            Uri uri = UriHelper.BuildUri(HukukTurkLowLevelClient.BaseUri, "Search", "Mevzuat", request.ToString());
            HttpResponseMessage response =
                await LowLevelClient.CallAsync(HttpMethod.Get, uri, ct: ct).ConfigureAwait(false);
            return await response.DeserializeAsync<SearchResponse<Mevzuat>>().ConfigureAwait(false);
        }

        /// <summary>
        /// Bir metne göre ilgili mevzuat araması yapar.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        public async Task<SearchResponse<Mevzuat>> SearchIlgiliMevzuatAsync(SearchIlgiliMevzuatRequest request,
            CancellationToken ct = default)
        {
            Uri uri = UriHelper.BuildUri(HukukTurkLowLevelClient.BaseUri, "Search", "IlgiliMevzuat",
                request.ToString());
            var pair = new KeyValuePair<string, string>(String.Empty, request.Metin);
            var content = new FormUrlEncodedContent(new[] {pair});
            HttpResponseMessage response = await LowLevelClient.CallAsync(HttpMethod.Post, uri, content, ct)
                .ConfigureAwait(false);
            return await response.DeserializeAsync<SearchResponse<Mevzuat>>().ConfigureAwait(false);
        }

        #endregion

        #region Async Options

        /// <summary>
        /// </summary>
        /// <param name="option"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        /// <exception cref="OperationCanceledException"></exception>
        public async Task<OptionsResponse> GetOptionsAsync(OptionName option, CancellationToken ct = default)
        {
            string methodName = Enum.GetName(option.GetType(), option);
            Uri uri = UriHelper.BuildUri(HukukTurkLowLevelClient.BaseUri, "Option", methodName);
            HttpResponseMessage response =
                await LowLevelClient.CallAsync(HttpMethod.Get, uri, ct: ct).ConfigureAwait(false);
            return await response.DeserializeAsync<OptionsResponse>().ConfigureAwait(false);
        }

        /// <summary>
        /// </summary>
        /// <param name="katalog"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        /// <exception cref="OperationCanceledException"></exception>
        public async Task<OptionsResponse> GetOptionsSiralamaAsync(int katalog, CancellationToken ct = default)
        {
            var query = $"katalog={katalog}";
            Uri uri = UriHelper.BuildUri(HukukTurkLowLevelClient.BaseUri, "Option", "Siralama", query);
            HttpResponseMessage response =
                await LowLevelClient.CallAsync(HttpMethod.Get, uri, ct: ct).ConfigureAwait(false);
            return await response.DeserializeAsync<OptionsResponse>().ConfigureAwait(false);
        }

        /// <summary>
        /// </summary>
        /// <param name="kararTuru"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        /// <exception cref="OperationCanceledException"></exception>
        public async Task<OptionsResponse> GetOptionsMerciAsync(int kararTuru, CancellationToken ct = default)
        {
            var query = $"kararturu={kararTuru}";
            Uri uri = UriHelper.BuildUri(HukukTurkLowLevelClient.BaseUri, "Option", "Merci", query);
            HttpResponseMessage response =
                await LowLevelClient.CallAsync(HttpMethod.Get, uri, ct: ct).ConfigureAwait(false);
            return await response.DeserializeAsync<OptionsResponse>().ConfigureAwait(false);
        }

        #endregion

        #region Sync Karar

        /// <summary>
        /// Yargıtay Kararı Arama
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public SearchResponse<Karar> SearchKararYargitay(SearchKararRequest request)
        {
            return SearchKararYargitayAsync(request).GetAwaiter().GetResult();
        }

        /// <summary>
        /// Danıştay Kararı Arama
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public SearchResponse<Karar> SearchKararDanistay(SearchKararRequest request)
        {
            return SearchKararDanistayAsync(request).GetAwaiter().GetResult();
        }

        /// <summary>
        /// Anayasa Mahkemesi Kararı Arama
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public SearchResponse<Karar> SearchKararAnayasaMahkemesi(SearchKararRequest request)
        {
            return SearchKararAnayasaMahkemesiAsync(request).GetAwaiter().GetResult();
        }

        /// <summary>
        /// Uyuşmazlık Mahkemesi Kararı Arama
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public SearchResponse<Karar> SearchKararUyusmazlikMahkemesi(SearchKararRequest request)
        {
            return SearchKararUyusmazlikMahkemesiAsync(request).GetAwaiter().GetResult();
        }

        /// <summary>
        /// Anayasa Mahkemesi Bireysel Başvuru Kararı Arama
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public SearchResponse<KararAnayasaBb> SearchKararAnayasaMahkemesiBb(SearchKararAnayasaBbRequest request)
        {
            return SearchKararAnayasaMahkemesiBbAsync(request).GetAwaiter().GetResult();
        }

        /// <summary>
        /// Avrupa İnsan Hakları Mahkemesi Kararı Arama
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public SearchResponse<KararAihm> SearchKararAihm(SearchKararAihmRequest request)
        {
            return SearchKararAihmAsync(request).GetAwaiter().GetResult();
        }

        /// <summary>
        /// Bir metne göre Yargıtay, Danıştay, Anayasa Mahkemesi ve Uyuşmazlık Mahkemesi içerisinde ilgili karar araması yapar.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SearchResponse<Karar> SearchIlgiliKararlar(SearchIlgiliKararlarRequest request)
        {
            return SearchIlgiliKararlarAsync(request).GetAwaiter().GetResult();
        }

        #endregion

        #region Sync Karar Maddeye Göre Arama

        /// <summary>
        ///     Belirtilen karar türünde Kanun-Madde ilişkisine göre
        ///     arama yapabilmek için Kanun listesini alır.
        /// </summary>
        /// <param name="kararTuru"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public KanunListesiResponse GetKararMaddeyeGoreAramaKanunListesi(int kararTuru)
        {
            return GetKararMaddeyeGoreAramaKanunListesiAsync(kararTuru).GetAwaiter().GetResult();
        }

        /// <summary>
        ///     Belirtilen karar türünde Kanun-Madde ilişkisine göre
        ///     arama yapabilmek için belirtilen mevzuat için Madde listesini alır.
        /// </summary>
        /// <param name="kararTuru"></param>
        /// <param name="mevzuatId"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public MaddeListesiResponse GetKararMaddeyeGoreAramaMaddeListesi(int kararTuru, int mevzuatId)
        {
            return GetKararMaddeyeGoreAramaMaddeListesiAsync(kararTuru, mevzuatId).GetAwaiter().GetResult();
        }

        /// <summary>
        ///     Belirtilen karar türünde Kanun-Madde ilişkisine göre
        ///     arama yapar.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public SearchResponse<Karar> KararMaddeyeGoreArama(KararMaddeyeGoreAramaRequest request)
        {
            return KararMaddeyeGoreAramaAsync(request).GetAwaiter().GetResult();
        }

        #endregion

        #region Sync Mevzuat

        /// <summary>
        /// T.C. Mevzuatı Arama
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public SearchResponse<Mevzuat> SearchMevzuat(SearchMevzuatRequest request)
        {
            return SearchMevzuatAsync(request).GetAwaiter().GetResult();
        }

        /// <summary>
        /// Bir metne göre ilgili mevzuat araması yapar.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SearchResponse<Mevzuat> SearchIlgiliMevzuat(SearchIlgiliMevzuatRequest request)
        {
            return SearchIlgiliMevzuatAsync(request).GetAwaiter().GetResult();
        }

        #endregion

        #region Sync Options

        /// <summary>
        /// </summary>
        /// <param name="option"></param>
        /// <returns></returns>
        public OptionsResponse GetOptions(OptionName option)
        {
            return GetOptionsAsync(option).GetAwaiter().GetResult();
        }

        /// <summary>
        /// </summary>
        /// <param name="katalog"></param>
        /// <returns></returns>
        public OptionsResponse GetOptionsSiralama(int katalog)
        {
            return GetOptionsSiralamaAsync(katalog).GetAwaiter().GetResult();
        }

        /// <summary>
        /// </summary>
        /// <param name="kararTuru"></param>
        /// <returns></returns>
        public OptionsResponse GetOptionsMerci(int kararTuru)
        {
            return GetOptionsMerciAsync(kararTuru).GetAwaiter().GetResult();
        }

        #endregion

        #region IDisposable

        protected virtual void Dispose(bool disposing)
        {
            if (disposing) LowLevelClient?.Dispose();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}