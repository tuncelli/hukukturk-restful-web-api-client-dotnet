﻿using HukukTurk.ServiceClient.Models;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace HukukTurk.ServiceClient
{
    internal static class HttpResponseMessageExtension
    {
        // ReSharper disable once ClassNeverInstantiated.Local
        private class ErrorResponse
        {
            // ReSharper disable once UnusedAutoPropertyAccessor.Local
            public string Message { get; set; }
        }

        internal static async Task<T> DeserializeAsync<T>(this HttpResponseMessage response) where T : IResponse
        {
            using (response)
            {
                T result;
                string json = await response.Content.ReadAsStringAsyncFixedAsync().ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    result = SerializationHelper.JsonDeserialize<T>(json);
                }
                else
                {
                    result = (T) Activator.CreateInstance(typeof(T), true);
                    ErrorResponse error = SerializationHelper.JsonDeserialize<ErrorResponse>(json);
                    result.ErrorMessage = error?.Message;
                }

                result.IsSuccess = response.IsSuccessStatusCode;
                result.StatusCode = (int) response.StatusCode;
                result.Reason = response.ReasonPhrase;
                return result;
            }
        }
    }
}