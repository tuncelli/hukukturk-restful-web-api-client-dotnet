﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
#if !NETCOREAPP1_1
using System.IO;
using System.Runtime.Serialization;
using System.Xml;
#endif

namespace HukukTurk.ServiceClient
{
    public static class SerializationHelper
    {
        private static readonly JsonSerializerSettings JsonSerializerSettings;

        static SerializationHelper()
        {
            JsonSerializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new DefaultContractResolver(),
                ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor
            };
        }

        public static string JsonSerialize(object value)
        {
            return JsonConvert.SerializeObject(value, JsonSerializerSettings);
        }

        public static T JsonDeserialize<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json, JsonSerializerSettings);
        }

#if !NETCOREAPP1_1
        public static string XmlSerialize(object value)
        {
            DataContractSerializer s = new DataContractSerializer(value.GetType());
            using (var sw = new StringWriter())
            {
                using (var xw = new XmlTextWriter(sw))
                {
                    s.WriteObject(xw, value);
                }

                return sw.ToString();
            }
        }

        public static T XmlDeserialize<T>(string xml)
        {
            DataContractSerializer s = new DataContractSerializer(typeof(T));
            using (var sr = new StringReader(xml))
            using (var xr = new XmlTextReader(sr))
            {
                return (T) s.ReadObject(xr);
            }
        }
#endif
    }
}