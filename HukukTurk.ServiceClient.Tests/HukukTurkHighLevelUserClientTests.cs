﻿using HukukTurk.ServiceClient.Models;
using NUnit.Framework;
using System.Threading.Tasks;

namespace HukukTurk.ServiceClient.Tests
{
    /// <remarks>
    /// Testleri çalıştırmadan önce BaseClassForTests sınıfındaki
    /// API giriş bilgilerini düzenleyiniz.
    /// </remarks>
    public class HukukTurkHighLevelUserClientTests : BaseClassForTests
    {
        private HukukTurkHighLevelUserClient Client => new HukukTurkHighLevelUserClient(AppId, UserId, UserSecret);

        [Test]
        public async Task SearchYargitayKararTestAsync()
        {
            SearchKararRequest request = new SearchKararRequest();
            request.Sorgu = "kıdem tazminatı";
            request.Sayfa = 40;
            SearchResponse<Karar> sonuc = await Client.SearchKararYargitayAsync(request);
            Assert.True(sonuc.Count > 0);
        }

        [Test]
        public async Task SearchDanistayKararTestAsync()
        {
            SearchKararRequest request = new SearchKararRequest();
            request.Sorgu = "kıdem tazminatı";
            SearchResponse<Karar> sonuc = await Client.SearchKararDanistayAsync(request);
            Assert.True(sonuc.Count > 0);
        }

        [Test]
        public async Task SearchAnayasaMahkemesiKararTestAsync()
        {
            SearchKararRequest request = new SearchKararRequest();
            request.Sorgu = "kıdem tazminatı";
            SearchResponse<Karar> sonuc = await Client.SearchKararAnayasaMahkemesiAsync(request);
            Assert.True(sonuc.Count > 0);
        }

        [Test]
        public async Task SearchUyusmazlikMahkemesiKararTestAsync()
        {
            SearchKararRequest request = new SearchKararRequest();
            request.Sorgu = "kıdem tazminatı";
            SearchResponse<Karar> sonuc = await Client.SearchKararUyusmazlikMahkemesiAsync(request);
            Assert.True(sonuc.Count > 0);
        }

        [Test]
        public async Task GetKararMaddeyeGoreAramaKanunListesiTestAsync()
        {
            int kararturu = 1471; // Yargıtay
            KanunListesiResponse sonuc = await Client.GetKararMaddeyeGoreAramaKanunListesiAsync(kararturu);
            Assert.True(sonuc.Count > 0);

            kararturu = 2501; // Yargıtay
            sonuc = await Client.GetKararMaddeyeGoreAramaKanunListesiAsync(kararturu);
            Assert.True(sonuc.Count > 0);

            kararturu = 1482; // Uyuşmazlık Mahkemesi
            sonuc = await Client.GetKararMaddeyeGoreAramaKanunListesiAsync(kararturu);
            Assert.True(sonuc.Count > 0);
        }

        [Test]
        public async Task GetKararMaddeyeGoreAramaMaddeListesiTestAsync()
        {
            int kararturu = 1471; // Yargıtay
            const int mevzuatid = 4547; // 4857 sy. İş Kanunu
            MaddeListesiResponse sonuc = await Client.GetKararMaddeyeGoreAramaMaddeListesiAsync(kararturu, mevzuatid);
            Assert.True(sonuc.Count > 0);

            kararturu = 2501; // Yargıtay
            sonuc = await Client.GetKararMaddeyeGoreAramaMaddeListesiAsync(kararturu, mevzuatid);
            Assert.True(sonuc.Count > 0);
        }

        [Test]
        public async Task KararMaddeyeGoreAramaAsyncTest()
        {
            int kararturu = 1471; // Yargıtay
            const int mevzuatid = 4547; // 4857 sy. İş Kanunu
            KararMaddeyeGoreAramaRequest request = new KararMaddeyeGoreAramaRequest();
            request.KararTuru = kararturu;
            request.MevzuatId = mevzuatid;
            request.MaddeNo = 1;
            request.MaddeTuru = 0;
            SearchResponse<Karar> sonuc = await Client.KararMaddeyeGoreAramaAsync(request);
            Assert.True(sonuc.Count > 0);
        }

        [Test]
        public async Task SearchIlgiliKararlarTestAsync()
        {
            const string metin = "Bilgisayarlarda, bilgisayar proğramlarında ve kütüklerinde arama, " +
                                 "kopyalama ve el koyma CMK 134. maddesinde düzenlenmiştir. Soruşturma " +
                                 "tarihinde yürürlükte bulunan madde içeriğine göre; bilgisayar kütüklerinin " +
                                 "şifresinin çözülememesinden dolayı girilememesi veya gizlenmiş bilgilere " +
                                 "ulaşılamaması halinde gerekli kopyalarının alınabilmesi için bu araç ve " +
                                 "gereçlere el konulabilir. Şifrenin çözümünün yapılması ve gerekli kopyaların " +
                                 "alınması halinde cihazlar gecikme olmaksızın iade edilir. Bilgisayar " +
                                 "kütüklerine el koyma işlemi sırasında sistemdeki bütün verilerin " +
                                 "yedeklemesi yapılır. İstemesi halinde, bu yedekten bir kopya çırakılarak " +
                                 "şüpheliye ve vekiline verilir. Genel kural bu şekilde olmakla birlikte " +
                                 "15 Temmuz 2016 yılında FETÖ/PDY mensupları tarafından gerçekleştirilen " +
                                 "darbeye teşebbüs suçu nedeniyle Anayasa hükümleri doğrultusunda ülke " +
                                 "genelinde olağanüstü hal ilan edilmiş ve bu süreçte uygulanacak hükümlere " +
                                 "ilişkin KHK'lar çıkarılmıştır. 668 sayılı KHK'nın 3. maddesinde;";
            SearchIlgiliKararlarRequest request = new SearchIlgiliKararlarRequest(metin);
            SearchResponse<Karar> sonuc = await Client.SearchIlgiliKararlarAsync(request);
            Assert.True(sonuc.Count > 0);
        }

        [Test]
        public async Task SearchAnayasaMahkemesiBbKararTestAsync()
        {
            SearchKararAnayasaBbRequest request = new SearchKararAnayasaBbRequest();
            request.Sorgu = "kıdem tazminatı";
            SearchResponse<KararAnayasaBb> sonuc = await Client.SearchKararAnayasaMahkemesiBbAsync(request);
            Assert.True(sonuc.Count > 0);
        }

        [Test]
        public async Task SearchAihmKararTestAsync()
        {
            SearchKararAihmRequest request = new SearchKararAihmRequest();
            request.Sorgu = "kıdem tazminatı";
            SearchResponse<KararAihm> sonuc = await Client.SearchKararAihmAsync(request);
            Assert.True(sonuc.Count > 0);
        }

        [Test]
        public async Task SearchMevzuatTestAsync()
        {
            SearchMevzuatRequest request = new SearchMevzuatRequest();
            request.Sorgu = "kıdem tazminatı";
            SearchResponse<Mevzuat> sonuc = await Client.SearchMevzuatAsync(request);
            Assert.True(sonuc.Count > 0);
        }

        [Test]
        public async Task SearchIlgiliMevzuatTestAsync()
        {
            const string metin = "İlk Derece ve Bölge Adliye Mahkemesinde silahların eşitliği ve " +
                            "çelişmeli yargılama ilkesi doğrultusunda savunmaya yeterli imkânın " +
                            "sağlanması ve bu hakkın etkin şekilde kullandırılmış olması, " +
                            "temyiz denetiminde sınırsız şekilde yazılı savunma imkânının " +
                            "kullanılabilme olanağının bulunması karşısında savunma hakkının " +
                            "kısıtlanması söz konusu olmadığından, 01.02.2018 tarihli ve " +
                            "7079 sayılı Kanunun 94. maddesi ile değişik CMK'nın 299/1. " +
                            "maddesi uyarınca takdiren duruşmalı inceleme taleplerinin";
            SearchIlgiliMevzuatRequest request = new SearchIlgiliMevzuatRequest(metin);
            request.MevzuatTuru = new[] {3342, 3345};
            SearchResponse<Mevzuat> sonuc = await Client.SearchIlgiliMevzuatAsync(request);
            Assert.True(sonuc.Count > 0);
        }

        [Test]
        public async Task GetOptionsAsyncTest()
        {
            OptionsResponse sonuc = await Client.GetOptionsAsync(OptionName.AmkDavaTuru);
            Assert.True(sonuc.Count > 0);
        }

        [Test]
        public async Task GetOptionsSiralamaAsyncTest()
        {
            const int katalog = 1469; // İçtihat
            OptionsResponse sonuc = await Client.GetOptionsSiralamaAsync(katalog);
            Assert.True(sonuc.Count > 0);
        }

        [Test]
        public async Task GetOptionsMerciAsyncTest()
        {
            const int kararTuru = 1471; // Yargıtay
            OptionsResponse sonuc = await Client.GetOptionsMerciAsync(kararTuru);
            Assert.True(sonuc.Count > 0);
        }
    }
}