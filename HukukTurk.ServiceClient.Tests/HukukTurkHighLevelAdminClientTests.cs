﻿using HukukTurk.ServiceClient.Models;
using NUnit.Framework;
using System.Threading.Tasks;

namespace HukukTurk.ServiceClient.Tests
{
    /// <remarks>
    /// Testleri çalıştırmadan önce BaseClassForTests sınıfındaki
    /// API giriş bilgilerini düzenleyiniz.
    /// </remarks>
    public class HukukTurkHighLevelAdminClientTests : BaseClassForTests
    {
        private HukukTurkHighLevelAdminClient Client => new HukukTurkHighLevelAdminClient(AppId, Secret);

        [Test]
        public async Task GetUsersTestAsync()
        {
            GetUsersResponse users = await Client.GetUsersAsync();
            Assert.AreEqual(UserId, users[0].Split(':')[0]);
        }

        [Test]
        public async Task AddRemoveUserTestAsync()
        {
            AddUserResponse addResponse = await Client.AddUserAsync();
            string userId = addResponse.UserId;
            Assert.IsTrue(userId.Length == 32);
            RemoveUserResponse removeResponse = await Client.RemoveUserAsync(userId);
            Assert.IsTrue(removeResponse.IsSuccess);
        }
    }
}