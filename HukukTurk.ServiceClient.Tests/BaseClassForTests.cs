﻿using NUnit.Framework;

namespace HukukTurk.ServiceClient.Tests
{
    public abstract class BaseClassForTests
    {
        public string AppId { get; set; }

        public string Secret { get; set; }

        public string UserId { get; set; }

        public string UserSecret { get; set; }

        [SetUp]
        public void Setup()
        {
            // API giriş bilgilerini burada ayarlayınız.
            AppId = "";
            Secret = "";
            UserId = "";
            UserSecret = "";
        }
    }
}